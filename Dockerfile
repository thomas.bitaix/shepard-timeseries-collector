FROM eclipse-temurin:17.0.9_9-jre

WORKDIR /app

COPY target/shepard-timeseries-collector-0.0.1-SNAPSHOT-jar-with-dependencies.jar stc.jar
COPY config /config

CMD ["java", "-jar", "stc.jar", "-c", "/config"]
