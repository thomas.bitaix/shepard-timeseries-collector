# shepard Timeseries Collector (sTC)

[Download JAR](https://gitlab.com/dlr-shepard/shepard-timeseries-collector/-/jobs/artifacts/main/raw/target/shepard-timeseries-collector-0.0.1-SNAPSHOT-jar-with-dependencies.jar?job=build)

[Documentation](https://gitlab.com/dlr-shepard/shepard-timeseries-collector/-/blob/main/docs/README.md)

[Maven Reports](https://dlr-shepard.gitlab.io/shepard-timeseries-collector/)

[Recommended JRE](https://adoptium.net/de/)
