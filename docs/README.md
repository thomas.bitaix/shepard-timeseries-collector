# Shepard Timeseries Collector (sTC)

The shepard timeseries collector aims to collect data from different sources and store them as timeseries data to shepard.

## Main concepts

sTC consists of three main blocks

- Sources

  Are responsible for gathering the data. Currently, sources for OPC/UA and KUKA RSI are implemented.

- Sinks

  Are responsible to store the data. Currently, a sink for shepard is implemented.

- Bridges

  Transfer data form sources to sinks. Single data points can be aggregated into chunks in order to reduce the load for the sink.

## Configuration

sTC is configured using YAML configuration files. By default, all files named `*.yml` in subfolder `config` are considered. Other configuration folders can be specified using the command line interface.

For more details on configuration please refer to the [configuration documentation](configuration.md)

## Running

A Java runtime environment (JRE) with at least version 17 is required. The [Adpoptium JRE/JDK](https://www.adoptium.net) is strongly recommended.

The program is delivered as Java archive (JAR) file and can be run as follows

```txt
java -jar shepard-timeseries-collector.jar
```

There are some command line options available, which can be queried using the `--help` command:

```txt
Usage: <main class> [-h] [--[no-]opcuaserver] [-c=<configFolder>]
                    [-l=<logLevel>] [-p=<opcuaServerPort>]
  -c, --configfolder=<configFolder>
                            Configuration folder
  -h, --help               Display help information
  -l, --loglevel=<logLevel>
                            Log level (one of debug, info, warn, error)
      --[no-]opcuaserver   Run OPC/UA server. True by default
  -p, --serverport=<opcuaServerPort>
                            OPC/UA server TCP port
```

As an alternative, you can also use docker/podman to run the program

```txt
docker run -it --rm -v ./config:/config registry.gitlab.com/dlr-shepard/shepard-timeseries-collector:latest
```
