# Configuration

[[_TOC_]]

## File format

Configuration files are in YAML format and should be named `*.yml`.

General structure:

```yaml
${item-name}:
  abstract: true|false
  extends: ${super-item}
  type: ${type}
  ${variable}: ${value}
```

- `${item-name}`

  Name of the item (source, bridge or sink). Each item must have a unique name (throughout all configuration files). Names must not contain spaces and should not contain other special characters.

- `abstract`

  If an item is marked as _abstract_, no attempts will be made to create an instance of the configured item. This is useful to create a configuration item which can be reused (inherited) by other items.

- `extends`

  Another item can be referenced by using its item-name. All configured values (with exception of the `abstract` keyword) from the specified super item are inherited. If the same parameter is configured both at the super item and the current item, the current item take precedence.

  Any other configuration item can be extended, not only abstract ones. A configuration which inherits from another configuration by default is not abstract (even if the super item is abstract). Multiple layers of inheritance are allowed, however the `abstract: true` configuration must be specified at each single item.

- `type`

  Each configuration item must specify a type. Possible types are listed below. Further types are possible with the integration of new sources, bridges or sinks.

- `${variable}`

  Depending on the type of the item, additional parameters may need to be configured.

## Sources

### Dummy Source

This dummy source generates random double values between -100 and +100 every two seconds. Can be used for debugging purposes.

Example configuration:

```yaml
dummy:
  type: dummy_source
```

### OPC/UA Source

Example configuration:

```yaml
opcua_source:
  type: OPCUA
  endpoint: opc.tcp://localhost:48010
  credentials:
    username: admin
    password: admin
  sampling_interval: 100
  publishing_interval: 100
  queue_size: 10
  capture_mode: subscription
  security_policy: Basic256Sha256
  nodeid: ns=0;i=2258
  polling_interval: 500
  substitution_value: 0
```

| Variable | Values | Description |
| --- | --- | --- |
| type | OPCUA | Must always be `OPCUA` |
| endpoint | URI | A valid URI to adress an OPC/UA server. The protocol is usually opc.tcp, Port 4840 |
| security_policy | refer to table | The security policy that is used to connect to the server. Can be `None` if the server accepts unencrypted connections. |
| credentials | username & password | If specified, the given username and password is used. If the credentials configuration item is missing, an anonymous connection is created. |
| capture_mode | subscription \| polling | Specify whether the nodes should be queried by explicit polling or by requesting a subscription from the OPC/UA server |
| nodeid | NodeId as string | Specify a single OPC/UA node as source. Namespace and ID must be provided. |
| nodepath | string | Browsepath including regular expressions (see below) |
| sampling_interval | integer | Interval in milliseconds (ms) at which the data should be sampled by the OPC/UA server |
| publishing_interval | integer | Interval ins milliseconds (ms) at which the OPC/UA server should transmit data to sTC |
| queue_size | integer | Size of the queue of the OPC/UA server to store data. If sampling interval is smaller than publishing interval, the queue must be large enough to store all values. |
| polling_interval | integer | Interval in milliseconds for active polling in polling mode. In subscription mode, active polling is performed if no new data has arrived for the specified amount of time. This allows to store data at regular intervals in subscription mode, even if the data values do not change. |
| substitution_value | ?? | _Not used at the moment?_ |

#### Security policy

One of

- None
- Basic128Rsa15
- Basic256
- Basic256Sha256
- Aes128_Sha256_RsaOep
- Aes128_Sha256_RsaPss

#### Node path

While a single node can be specified using the `nodeid` configuration, multiple nodes can be selected at one by specifying a path to locate nodes. The path is based on the browsename property of the nodes, using the `Organizes` and `HasComponent` relationships. The node path can contain regular expressions to match multiple nodes. The regular expressions are contained in double brackets `{{` and `}}` and an additional variable name for later identification. Parts of the path are separated by a slash `/`. Regular expressions must match a whole part of the paths and cannot span multiple path. (That is there is no `**` kind of operator).

Example:

```yaml
node_path: "Objects/BuildingAutomation/{{ AirConditioner_[0-9]+ | variable }}/Temperature"
```

Beginning from the root of the node tree, first the `Objects` node and then the `BuildingAutomation` nodes are retrieved. Starting at this point, all child nodes are evaluated according to the regular expression `AirConditioner_[0-9]+`. All nodes that are found are retrieved, and relative to that node, a child `Temperature` is searched. If this node is found, its NodeID is stored in the list of nodes to query. Additionally, a variable named `variable` is stored with that name of the node fulfilling the regular expression (e.g. `AirConditioner_8`).

Browsing the node tree is only performed upon startup of the application. If the OPC/UA server later adds nodes that would match the specified nodepath, they will not be queried until sTC is restarted.

## Bridges

Example configuration:

```yaml
temperature_cube2:
  type: stcBridge
  source_id: temperature
  sink_id: shepard_cube2
  queue_size: 1000
  mapping:
    measurement: "{{ variable }}_meas"
    location: MFZ
    device: R10
    symbolic_name: A4
    field: hPa
  value_template: "$abs($floor(value))"
```

| Variable | Values | Description |
| --- | --- | --- |
| type | stcBridge | Must always be `stcBridge` |
| source_id | Source ID | A valid source_id |
| sink_id | Sink ID | A valid sink_id |
| queue_size | Number | The maximum size of the queue before the next cycle will clear it and send the data to the given sinks |
| queue_duration | Milliseconds | The maximum age of the queue before the next cycle will clear it and send the data to the given sinks |
| mapping | Timeseries Mapping | The values to be used for creating shepard timeseries. These values can be hard coded or variables can be used. |
| value_template | [JSONata Syntax](https://jsonata.org/) | JSONata syntax for manipulating the value while it's passing the bridge |

### Timeseries Mapping

In simple cases, timeseries mapping can be hardcoded, but in more complicated cases, such as when regular expressions are used in node paths, the mapping can no longer be hardcoded. The variables given by the respective sinks can be used then by surrounding them with double curly braces (`{{ variable_name }}`). Variables can be combined with hardcoded values as seen in the given example. The `field` is optional since it can also be set by the sink itself.

### Value Template

The `value_template` can be used to manipulate the value as it passes the bridge. The manipulation is done via [JSONata syntax](https://jsonata.org/) and assumes that there is a function that accepts exactly one variable called `value`. The output of this function is then used for the sinks.

## Sinks

### Dummy Sink

The dummy sink logs received events with the log level `INFO`. Can be used for debugging purposes.

Example configuration:

```yaml
dummy:
  type: dummy_sink
```

### Shepard Sink

Example configuration:

```yaml
shepard_cube3:
  type: shepard
  host: https://backend.bt-au-cube3.intra.dlr.de/shepard/api
  api_key: bhjjlvfsdanjmklklvcfdejnklvcfdesjoklkvclfdamkjlcvöeaanvjofeavcmrdfo
  timeseries_container_id: 5432
  field: value
```

| Variable | Values | Description |
| --- | --- | --- |
| type | shepard | Must always be `shepard` |
| host | URI | URI of the shepard api (usually ends with `.../shepard/api`) |
| api_key | JWT | A valid api key |
| timeseries_container_id | Number | The ID of an existing time series container into which data is to be written |
| field | string | The default field to use for the timeseries to be written if no field is set in the timeseries mapping |
