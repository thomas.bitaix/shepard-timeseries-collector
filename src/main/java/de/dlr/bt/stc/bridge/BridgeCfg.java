package de.dlr.bt.stc.bridge;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.ACfg;
import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;

public class BridgeCfg extends ACfg {

	protected BridgeCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("stcBridge", BridgeCfg::new);
	}

	public String getSourceId() {
		return config.getString("source_id");
	}

	public String getSinkId() {
		return config.getString("sink_id");
	}

	public Integer getQueueSize() {
		return config.getInt("queue_size", -1);
	}

	public Integer getQueueDuration() {
		return config.getInt("queue_duration", -1);
	}

	public MappingCfg getMapping() {
		return new MappingCfg(config.configurationAt("mapping"));
	}

	public String getValueTemplate() {
		return config.getString("value_template", "value");
	}

}
