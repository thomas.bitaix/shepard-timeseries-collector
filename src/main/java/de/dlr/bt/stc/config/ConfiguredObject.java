package de.dlr.bt.stc.config;

import java.util.Objects;

import org.apache.commons.configuration2.HierarchicalConfiguration;

public abstract class ConfiguredObject {
	protected final HierarchicalConfiguration<?> config;

	protected ConfiguredObject(HierarchicalConfiguration<?> config) {
		this.config = Objects.requireNonNull(config);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		var it = config.getKeys();
		while (it.hasNext()) {
			var element = it.next();
			sb.append(element);
			sb.append("=\"");
			sb.append(config.getString(element));
			sb.append("\"");
			if (it.hasNext())
				sb.append(",");
		}
		sb.append("}");
		return sb.toString();
	}
}
