package de.dlr.bt.stc.entities;

import de.dlr.bt.stc.task.ITask;
import lombok.Data;
import lombok.NonNull;

@Data
public class TaskLifecycle<T extends ITask> {
	@NonNull
	final T task;
	boolean initialized = false;
	boolean started = false;
}
