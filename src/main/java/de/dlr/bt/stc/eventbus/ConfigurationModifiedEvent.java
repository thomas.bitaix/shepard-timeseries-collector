package de.dlr.bt.stc.eventbus;

import de.dlr.bt.stc.config.ACfg;
import lombok.Value;

@Value
public class ConfigurationModifiedEvent {
	private String key;
	private ACfg configuration;
	private ConfigurationModificationType modification;

	public enum ConfigurationModificationType {
		ADD, MODIFY, REMOVE
	}
}
