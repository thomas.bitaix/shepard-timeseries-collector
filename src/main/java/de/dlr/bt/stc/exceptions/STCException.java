package de.dlr.bt.stc.exceptions;

import lombok.experimental.StandardException;

@StandardException
public class STCException extends Exception {
	private static final long serialVersionUID = 3159667062522717451L;

}
