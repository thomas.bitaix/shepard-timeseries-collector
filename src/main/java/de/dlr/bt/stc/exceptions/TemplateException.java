package de.dlr.bt.stc.exceptions;

import lombok.experimental.StandardException;

@StandardException
public class TemplateException extends STCException {
	private static final long serialVersionUID = -2649460240296509711L;
}
