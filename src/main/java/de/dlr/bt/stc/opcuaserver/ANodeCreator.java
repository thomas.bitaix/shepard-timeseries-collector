package de.dlr.bt.stc.opcuaserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.milo.opcua.sdk.server.nodes.UaNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;

public abstract class ANodeCreator implements INodeCreator {
	private final List<String> basePath;
	protected final STCNamespace namespace;

	protected static final NodeId NULLNODEID = new NodeId(0, 0);

	protected ANodeCreator(STCNamespace namespace, List<String> basePath) {
		this.basePath = basePath;
		this.namespace = namespace;
	}

	protected ANodeCreator(STCNamespace namespace, String firstPathElement, String... basePath) {
		this.basePath = new ArrayList<>();
		this.basePath.add(firstPathElement);
		this.basePath.addAll(Arrays.asList(basePath));
		this.namespace = namespace;
	}

	protected String[] nodePathInst(String... name) {
		ArrayList<String> res = new ArrayList<>();
		res.addAll(basePath);
		res.addAll(Arrays.asList(name));

		return res.toArray(String[]::new);
	}

	protected String[] nodePathType(String... name) {
		ArrayList<String> res = new ArrayList<>();
		res.addAll(basePath);
		res.add("type");
		res.addAll(Arrays.asList(name));

		return res.toArray(String[]::new);
	}

	private final Map<Object, UaNode> rootNodes = new HashMap<>();

	@Override
	public void removeInstance(Object forNode, Folders folders) {
		var rootNode = rootNodes.get(forNode);
		if (rootNode != null) {
			namespace.removeObjectNode(rootNode);
			rootNodes.remove(forNode);
		}
	}

	protected void addRootNode(Object forNode, UaNode rootNode) {
		rootNodes.put(forNode, rootNode);
	}
}
