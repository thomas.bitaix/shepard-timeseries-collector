package de.dlr.bt.stc.opcuaserver;

import java.security.cert.X509Certificate;
import java.util.List;

import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.server.security.ServerCertificateValidator;

public class AcceptAllCertificateValidator implements ServerCertificateValidator {

	@Override
	public void validateCertificateChain(List<X509Certificate> certificateChain) throws UaException {
		// Do nothing, accept any certificate as valid
	}

	@Override
	public void validateCertificateChain(List<X509Certificate> certificateChain, String applicationUri)
			throws UaException {
		// Do nothing, accept any certificate as valid
	}

}
