package de.dlr.bt.stc.opcuaserver.method;

import org.eclipse.milo.opcua.sdk.client.methods.UaMethodException;
import org.eclipse.milo.opcua.sdk.core.ValueRanks;
import org.eclipse.milo.opcua.sdk.server.api.methods.AbstractMethodInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.init.STCInstance;

public class StartMethod extends AbstractMethodInvocationHandler {

	private EventBus eventBus;

	public StartMethod(UaMethodNode node, EventBus eventBus) {
		super(node);
		this.eventBus = eventBus;
	}

	@Override
	public Argument[] getInputArguments() {
		return new Argument[] { new Argument("restartInterval", Identifiers.Int32, ValueRanks.Scalar, null,
				LocalizedText.english("Restart Interval")) };
	}

	@Override
	public Argument[] getOutputArguments() {
		return new Argument[0];
	}

	@Override
	protected Variant[] invoke(InvocationContext invocationContext, Variant[] inputValues) throws UaException {
		var intervalinput = inputValues[0].getValue();
		if (intervalinput instanceof Integer intiv && intiv >= 0)
			eventBus.post(new STCInstance.StartEvent(intiv));
		else
			throw new UaMethodException(StatusCode.BAD, null, null);
		return new Variant[] {};
	}

}
