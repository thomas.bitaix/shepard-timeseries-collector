package de.dlr.bt.stc.opcuaserver.method;

import org.eclipse.milo.opcua.sdk.server.api.methods.AbstractMethodInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.init.STCInstance.UnloadConfiguration;

public class UnloadConfigurationMethod extends AbstractMethodInvocationHandler {

	private EventBus eventBus;

	public UnloadConfigurationMethod(UaMethodNode node, EventBus eventBus) {
		super(node);
		this.eventBus = eventBus;
	}

	@Override
	public Argument[] getInputArguments() {
		return new Argument[0];
	}

	@Override
	public Argument[] getOutputArguments() {
		return new Argument[0];
	}

	@Override
	protected Variant[] invoke(InvocationContext invocationContext, Variant[] inputValues) throws UaException {
		eventBus.post(new UnloadConfiguration());
		return new Variant[] {};
	}

}
