package de.dlr.bt.stc.sink.dummy;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.eventbus.CacheFullEvent;
import de.dlr.bt.stc.sink.ASink;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummySink extends ASink {

	public DummySink(String key, SinkDummyCfg cfg, EventBus eventBus) {
		super(key, eventBus, cfg.getField());
	}

	@Override
	protected void handleEvent(CacheFullEvent event) {
		var fieldToSet = decideForField(event.getTimeseries().getField());
		if (fieldToSet == null) {
			log.error("No field configured for timeseries {} in sink {}", event.getTimeseries(), key);
		}
		log.info("Received CacheFullEvent in sink {}: {} with field {}", key, event.toString(), fieldToSet);
	}

	@Override
	protected boolean validate() {
		return true;
	}

}