package de.dlr.bt.stc.sink.dummy;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.sink.ASinkCfg;

public class SinkDummyCfg extends ASinkCfg {

	protected SinkDummyCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("dummy_sink", SinkDummyCfg::new);
	}

}
