package de.dlr.bt.stc.sink.shepard;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.sink.ASinkProvider;
import de.dlr.bt.stc.task.TaskProviderFactory;

public class ShepardSinkProvider extends ASinkProvider<ShepardSink> {

	@Register
	public static void register() {
		TaskProviderFactory.getInstance().registerCreator(SinkShepardCfg.class, ShepardSinkProvider::new);
	}

	private ShepardSinkProvider(ConfigurationManager cfg) {
		super(cfg.getManagementEventBus());
		for (var entry : cfg.getConfigurations().entrySet()) {
			if (entry.getValue() instanceof SinkShepardCfg scfg) {
				putSink(entry.getKey(), new ShepardSink(entry.getKey(), scfg, cfg.getInstanceEventBus()));
			}
		}
	}

}
