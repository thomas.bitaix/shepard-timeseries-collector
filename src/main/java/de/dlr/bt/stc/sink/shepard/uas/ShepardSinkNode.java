package de.dlr.bt.stc.sink.shepard.uas;

import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.sdk.server.nodes.filters.AttributeFilters;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.opcuaserver.ANodeCreator;
import de.dlr.bt.stc.opcuaserver.NodeFactory;
import de.dlr.bt.stc.opcuaserver.STCNamespace;
import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;
import de.dlr.bt.stc.sink.shepard.ShepardSink;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShepardSinkNode extends ANodeCreator {
	private static final String SINK_TYPE = "ShepardSinkType";

	private static final String QUEUE_SIZE = "QueueSize";
	private static final String ACTIVE_COUNT = "ActiveCount";
	private static final String POOL_SIZE = "PoolSize";
	private static final String LARGEST_POOL_SIZE = "LargestPoolSize";

	private static final String HOST = "Host";
	private static final String TIMESERIES_ID = "TimeseriesID";

	private UaObjectTypeNode typeNode;

	@Register
	public static void register() {
		NodeFactory.getInstance().registerCreator(ShepardSink.class, ShepardSinkNode::new);
	}

	private ShepardSinkNode(STCNamespace namespace) {
		super(namespace, "stc", "sinks", "shepardsink");
	}

	@Override
	public void createObjectType() {
		var queueSize = namespace.createObjectTypeComponent(QUEUE_SIZE, namespace.newNodeId(nodePathType(QUEUE_SIZE)),
				Identifiers.Int32);
		var activeCount = namespace.createObjectTypeComponent(ACTIVE_COUNT,
				namespace.newNodeId(nodePathType(ACTIVE_COUNT)), Identifiers.Int32);
		var poolSize = namespace.createObjectTypeComponent(POOL_SIZE, namespace.newNodeId(nodePathType(POOL_SIZE)),
				Identifiers.Int32);
		var largestPoolSize = namespace.createObjectTypeComponent(LARGEST_POOL_SIZE,
				namespace.newNodeId(nodePathType(LARGEST_POOL_SIZE)), Identifiers.Int32);

		var host = namespace.createObjectTypeComponent(HOST, namespace.newNodeId(nodePathType(HOST)),
				Identifiers.String);
		var timeseriesid = namespace.createObjectTypeComponent(TIMESERIES_ID,
				namespace.newNodeId(nodePathType(TIMESERIES_ID)), Identifiers.Int64);

		typeNode = namespace.createObjectTypeNode(SINK_TYPE, namespace.newNodeId(nodePathType(SINK_TYPE)), queueSize,
				activeCount, poolSize, largestPoolSize, host, timeseriesid);
	}

	@Override
	public void createInstance(Object forNode, Folders folders) {
		if (!(forNode instanceof ShepardSink shepardSink))
			return;

		try {
			String id = shepardSink.getKey();
			var uon = namespace.createObjectNode(typeNode, id, namespace.newNodeId(nodePathInst(id)),
					folders.getSinkFolder());

			var queueSizeFilter = AttributeFilters.getValue(ctx -> dataValueFromInt(shepardSink.getQueueSize()));
			namespace.getObjectNodeComponent(uon, QUEUE_SIZE).getFilterChain().addLast(queueSizeFilter);
			var activeCountFilter = AttributeFilters
					.getValue(ctx -> dataValueFromInt(shepardSink.getExecutorActiveCount()));
			namespace.getObjectNodeComponent(uon, ACTIVE_COUNT).getFilterChain().addLast(activeCountFilter);
			var poolSizeFilter = AttributeFilters.getValue(ctx -> dataValueFromInt(shepardSink.getExecutorPoolSize()));
			namespace.getObjectNodeComponent(uon, POOL_SIZE).getFilterChain().addLast(poolSizeFilter);
			var largestPoolSizeFilter = AttributeFilters
					.getValue(ctx -> dataValueFromInt(shepardSink.getExecutorLargestPoolSize()));
			namespace.getObjectNodeComponent(uon, LARGEST_POOL_SIZE).getFilterChain().addLast(largestPoolSizeFilter);

			namespace.setObjectNodeComponent(uon, HOST, new Variant(shepardSink.getHost()));
			namespace.setObjectNodeComponent(uon, TIMESERIES_ID, new Variant(shepardSink.getTimeseriesContainerId()));

			addRootNode(forNode, uon);
		} catch (UaException e) {
			log.info("Exception during creation of NodeInstance {}: {}", shepardSink.getKey(), e);
		}
	}

	private DataValue dataValueFromInt(int value) {
		return new DataValue(new Variant(value));
	}

}
