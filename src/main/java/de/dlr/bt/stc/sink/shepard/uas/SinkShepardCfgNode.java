package de.dlr.bt.stc.sink.shepard.uas;

import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.opcuaserver.ANodeCreator;
import de.dlr.bt.stc.opcuaserver.NodeFactory;
import de.dlr.bt.stc.opcuaserver.STCNamespace;
import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;
import de.dlr.bt.stc.sink.shepard.SinkShepardCfg;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SinkShepardCfgNode extends ANodeCreator {
	@Register
	public static void register() {
		NodeFactory.getInstance().registerCreator(SinkShepardCfg.class, SinkShepardCfgNode::new);
	}

	private SinkShepardCfgNode(STCNamespace namespace) {
		super(namespace, "stc", "sinks", "sinkshepardcfg");
	}

	private UaObjectTypeNode typeNode;

	private static final String SINKSHEPARDCFG_TYPE = "SinkShepardCfgType";

	private static final String HOST = "Host";
	private static final String TIMESERIES_ID = "TimeseriesID";

	@Override
	public void createObjectType() {
		var host = namespace.createObjectTypeComponent(HOST, namespace.newNodeId(nodePathType(HOST)),
				Identifiers.String);
		var timeseriesid = namespace.createObjectTypeComponent(TIMESERIES_ID,
				namespace.newNodeId(nodePathType(TIMESERIES_ID)), Identifiers.Int64);

		typeNode = namespace.createObjectTypeNode("SinkShepardCfg",
				namespace.newNodeId(nodePathType(SINKSHEPARDCFG_TYPE)), host, timeseriesid);
	}

	@Override
	public void createInstance(Object forNode, Folders folders) {
		if (!(forNode instanceof SinkShepardCfg sinkShepardCfg))
			return;

		try {
			String id = sinkShepardCfg.getId();
			var uon = namespace.createObjectNode(typeNode, id, namespace.newNodeId(nodePathInst(id)),
					folders.getConfigFolder());

			namespace.setObjectNodeComponent(uon, HOST, new Variant(sinkShepardCfg.getHost()));
			namespace.setObjectNodeComponent(uon, TIMESERIES_ID,
					new Variant(sinkShepardCfg.getTimeseriesContainerId()));

			addRootNode(forNode, uon);
		} catch (UaException e) {
			log.info("Exception during creation of NodeInstance {}: {}", sinkShepardCfg.getId(), e);
		}

	}

}
