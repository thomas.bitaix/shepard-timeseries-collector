package de.dlr.bt.stc.source;

import javax.annotation.Nullable;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationRuntimeException;

public abstract class AEndpointSourceCfg extends ASourceCfg {

	protected AEndpointSourceCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@Nullable
	public CredentialsCfg getCredentials() {
		try {
			return new CredentialsCfg(config.configurationAt("credentials"));
		} catch (ConfigurationRuntimeException cre) {
			return null;
		}
	}

	@Nullable
	public String getEndpoint() {
		return config.getString("endpoint");
	}

}
