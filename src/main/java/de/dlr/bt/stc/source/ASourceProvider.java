package de.dlr.bt.stc.source;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.task.ATaskProvider;
import de.dlr.bt.stc.task.ISourceProvider;
import de.dlr.bt.stc.task.ITask;

public class ASourceProvider<T extends ITask> extends ATaskProvider<T> implements ISourceProvider {

	protected ASourceProvider(EventBus managementEventBus) {
		super(managementEventBus);
	}

}
