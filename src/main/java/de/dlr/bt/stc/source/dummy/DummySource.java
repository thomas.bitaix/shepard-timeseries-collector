package de.dlr.bt.stc.source.dummy;

import java.util.Collections;
import java.util.Random;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.source.ISource;
import de.dlr.bt.stc.util.DateHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummySource extends Thread implements ISource {

	private int loopTime = 2000;
	private boolean running = false;
	private final EventBus eventBus;
	private final Random random = new Random();
	private final String key;

	private final DateHelper dateHelper = new DateHelper();

	public DummySource(String key, EventBus eventBus) {
		this.key = key;
		this.eventBus = eventBus;
	}

	@Override
	public void run() {
		running = true;
		while (running) {
			var value = random.nextDouble(-100, 100);
			var timestamp = dateHelper.getDate().getTime() * (long) 1e6;
			var event = new DataAvailableEvent(key, value, timestamp, Collections.emptyMap());
			log.info("New dummy event in source {}: {}", key, event);
			eventBus.post(event);

			try {
				Thread.sleep(loopTime);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}

		}
	}

	@Override
	public void initializeTask() throws ConfigurationException {
		log.debug("Initializing source {}", key);
	}

	@Override
	public void startTask() {
		log.debug("Starting source {}", key);
		this.start();
	}

	@Override
	public void stopTask() {
		log.debug("Stopping source {}", key);
		running = false;
	}

}
