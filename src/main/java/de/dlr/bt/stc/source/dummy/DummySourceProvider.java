package de.dlr.bt.stc.source.dummy;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.entities.TaskLifecycle;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.ASourceProvider;
import de.dlr.bt.stc.task.TaskProviderFactory;

public class DummySourceProvider extends ASourceProvider<DummySource> {

	@Register
	public static void register() {
		TaskProviderFactory.getInstance().registerCreator(SourceDummyCfg.class, DummySourceProvider::new);
	}

	private DummySourceProvider(ConfigurationManager cfg) {
		super(cfg.getManagementEventBus());
		for (var entry : cfg.getConfigurations().entrySet()) {
			if (entry.getValue() instanceof SourceDummyCfg) {
				taskLifecycles.put(entry.getKey(),
						new TaskLifecycle<>(new DummySource(entry.getKey(), cfg.getInstanceEventBus())));
			}
		}
	}
}
