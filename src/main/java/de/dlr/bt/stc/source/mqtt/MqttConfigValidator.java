package de.dlr.bt.stc.source.mqtt;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;

public class MqttConfigValidator {
  
  private static final String NOT_VALID = "Source Configuration not valid: ";
  
  public static void validateSourceConfiguration(SourceMQTTCfg sourceConfiguration)
      throws SourceConfigurationException {
    if (sourceConfiguration == null) {
      throw new SourceConfigurationException(NOT_VALID + "No configuation set for MQTT");
    }
    if (sourceConfiguration.getEndpoint() == null) {
      throw new SourceConfigurationException(NOT_VALID + "No endpoint set for MQTT");
    }
    if (sourceConfiguration.getTopic() == null) {
      throw new SourceConfigurationException(NOT_VALID + "No topic set for MQTT");
    }
    if (sourceConfiguration.getId() == null) {
      throw new SourceConfigurationException(NOT_VALID + "No id set for MQTT");
    }
  }

}
