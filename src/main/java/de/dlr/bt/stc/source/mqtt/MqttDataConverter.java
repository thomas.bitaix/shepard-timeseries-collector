package de.dlr.bt.stc.source.mqtt;

import com.google.common.base.Charsets;

import de.dlr.bt.stc.source.rsi.SourceRSICfg.SourceDataType;

public class MqttDataConverter {
  
  public static Object convertPayload(final byte[] payload, final SourceDataType dataType) {
    String data = new String(payload, Charsets.UTF_8 );
    if (dataType == null)
      return data;
    try {
      return switch (dataType) {
        case BOOL -> Boolean.valueOf(data);
        case FLOAT -> Double.valueOf(data);
        case INTEGER -> Long.valueOf(data);
        default -> data;
      };
    } catch (NumberFormatException e) {
      throw new RuntimeException("Problems converting message payload. ", e);
    }
  }
}
