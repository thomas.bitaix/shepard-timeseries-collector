package de.dlr.bt.stc.source.opcua;

import java.util.Objects;

import javax.annotation.Nullable;

import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;

import de.dlr.bt.stc.source.CredentialsCfg;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class MiloClientCfg {
	private SourceOPCUACfg sourcecfg;

	public MiloClientCfg(SourceOPCUACfg sourcecfg) {
		this.sourcecfg = Objects.requireNonNull(sourcecfg);
	}

	@Nullable
	@ToString.Include
	public String getEndpoint() {
		return sourcecfg.getEndpoint();
	}

	@EqualsAndHashCode.Include
	protected String getEndpointLC() {
		return getEndpoint().toLowerCase();
	}

	@EqualsAndHashCode.Include
	@Nullable
	public CredentialsCfg getCredentials() {
		return sourcecfg.getCredentials();
	}

	@ToString.Include
	@EqualsAndHashCode.Include
	public SecurityPolicy getSecurityPolicy() {
		return sourcecfg.getSecurityPolicy();
	}

	@ToString.Include
	@EqualsAndHashCode.Include
	@Nullable
	public String getKeystorePath() {
		return sourcecfg.getKeystorePath();
	}

	@EqualsAndHashCode.Include
	@Nullable
	public String getKeystorePassword() {
		return sourcecfg.getKeystorePassword();
	}

	@EqualsAndHashCode.Include
	public boolean isCreateKeystore() {
		return sourcecfg.isCreateKeystore();
	}

}
