package de.dlr.bt.stc.source.opcua;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import de.dlr.bt.stc.source.opcua.NodeItems.NodeItem;
import lombok.Data;

public class NodeItems extends HashMap<NodeId, NodeItem> {
	private static final long serialVersionUID = 5379404054690811818L;

	@Data
	public static class NodeItem {
		private final Map<String, String> varMap;
		private Instant lastUpdate;
		private DataValue lastValue = null;

		public NodeItem() {
			this(new HashMap<>());
		}

		public NodeItem(Map<String, String> varMap) {
			this.varMap = new HashMap<>(varMap);
			this.lastUpdate = Instant.now();
		}
	}
}
