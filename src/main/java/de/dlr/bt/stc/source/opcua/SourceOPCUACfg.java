package de.dlr.bt.stc.source.opcua;

import javax.annotation.Nullable;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.AEndpointSourceCfg;

public class SourceOPCUACfg extends AEndpointSourceCfg {

	protected SourceOPCUACfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	private static SourceOPCUACfg createInstance(HierarchicalConfiguration<?> config) {
		return new SourceOPCUACfg(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("opcua", SourceOPCUACfg::createInstance);
	}

	/**
	 * Requested sampling interval in ms
	 */
	@Nullable
	public Double getSamplingInterval() {
		return config.getDouble("sampling_interval", null);
	}

	/**
	 * Requested publishing interval in ms
	 */
	@Nullable
	public Double getPublishingInterval() {
		return config.getDouble("publishing_interval", null);
	}

	/**
	 * Queue size for publishing, should be large enough to carry all DataValues if
	 * sampling interval is smaller than publishing interval
	 */
	@Nullable
	public Integer getQueueSize() {
		return config.getInteger("queue_size", null);
	}

	@Nullable
	public Integer getPollingInterval() {
		return config.getInteger("polling_interval", null);
	}

	@Nullable
	public String getSubstitutionValue() {
		return config.getString("substitution_value", null);
	}

	@Nullable
	public String getNodeId() {
		return config.getString("nodeid", null);
	}

	@Nullable
	public String getNodePath() {
		return config.getString("node_path", null);
	}

	public CaptureMode getCaptureMode() {
		var cm = config.getString("capture_mode", "subscription");
		return CaptureMode.valueOf(cm.toUpperCase());
	}

	public SecurityPolicy getSecurityPolicy() {
		var sp = config.getString("security_policy", "None");
		return SecurityPolicy.valueOf(sp);
	}

	@Nullable
	public String getKeystorePath() {
		return config.getString("keystore_path", System.getProperty("java.io.tmpdir") + "/stckst.pk12");
	}

	@Nullable
	public String getKeystorePassword() {
		return config.getString("keystore_password", "kpw");
	}

	public boolean isCreateKeystore() {
		return config.getBoolean("keystore_create", true);
	}

	public enum CaptureMode {
		SUBSCRIPTION, POLLING
	}
}
