package de.dlr.bt.stc.source.opcua;

import lombok.Value;

@Value
public class SubscriptionParameters {
	private double samplingInterval;
	private double publishingInterval;
	private int queueSize;
}
