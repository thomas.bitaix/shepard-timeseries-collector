package de.dlr.bt.stc.source.rsi;

import java.util.HashMap;
import java.util.Map;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.entities.TaskLifecycle;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.task.ATaskProvider;
import de.dlr.bt.stc.task.ISourceProvider;
import de.dlr.bt.stc.task.TaskProviderFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RSISourceProvider extends ATaskProvider<RSISource> implements ISourceProvider {

	private final EventBus instanceEventBus;

	private final Map<RSIClientCfg, RSIClient> rsiClients = new HashMap<>();

	@Register
	public static void register() {
		TaskProviderFactory.getInstance().registerCreator(SourceRSICfg.class, RSISourceProvider::new);
	}

	protected RSISourceProvider(ConfigurationManager config) {
		super(config.getManagementEventBus());
		this.instanceEventBus = config.getInstanceEventBus();

		for (var entry : config.getConfigurations().entrySet()) {
			if (entry.getValue() instanceof SourceRSICfg src) {
				try {
					this.taskLifecycles.put(src.getId(),
							new TaskLifecycle<>(new RSISource(src, this.instanceEventBus)));
					this.rsiClients.putIfAbsent(new RSIClientCfg(src), null);
				} catch (SourceConfigurationException ex) {
					log.error("Could not create RSISource {}, failed with exception {}", entry.getKey(), ex);
				}
			}
		}
	}

	@Override
	public boolean initializeTasks() {
		boolean ok = createRSIClients();
		ok &= super.initializeTasks();

		return ok;
	}

	@Override
	protected boolean initializeTask(TaskLifecycle<RSISource> task) {
		if (task.getTask().getClient() == null) {
			RSIClientCfg mcfg = new RSIClientCfg(task.getTask().getConfig());
			var rc = rsiClients.get(mcfg);
			if (rc != null)
				task.getTask().setClient(rc);

		}

		if (task.getTask().getClient() != null)
			return super.initializeTask(task);
		else
			return false;

	}

	private boolean createRSIClients() {
		boolean ok = true;
		for (var entry : rsiClients.entrySet()) {
			if (entry.getValue() == null) {
				try {
					var client = new RSIClient(entry.getKey());
					entry.setValue(client);
				} catch (SourceConfigurationException sce) {
					ok = false;
					log.error("Could not initialize RSIClient {}, Exception {}", entry, sce.getMessage());
				}
			}
		}
		return ok;
	}

	@Override
	public void cleanupTasks() {
		stopTasks();

		super.cleanupTasks();

		for (var entry : rsiClients.entrySet()) {
			if (entry.getValue() != null) {
				entry.getValue().cleanup();
				entry.setValue(null);
			}
		}
	}
}
