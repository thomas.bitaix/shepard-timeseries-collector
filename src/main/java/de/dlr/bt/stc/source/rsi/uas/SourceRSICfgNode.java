package de.dlr.bt.stc.source.rsi.uas;

import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.opcuaserver.ANodeCreator;
import de.dlr.bt.stc.opcuaserver.NodeFactory;
import de.dlr.bt.stc.opcuaserver.STCNamespace;
import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;
import de.dlr.bt.stc.source.rsi.SourceRSICfg;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SourceRSICfgNode extends ANodeCreator {
	@Register
	public static void register() {
		NodeFactory.getInstance().registerCreator(SourceRSICfg.class, SourceRSICfgNode::new);
	}

	private SourceRSICfgNode(STCNamespace namespace) {
		super(namespace, "stc", "sources", "sourcersicfg");
	}

	private UaObjectTypeNode typeNode;

	private static final String SOURCERSICFG_TYPE = "SourceRSICfgType";

	private static final String PORT = "Port";
	private static final String DATATYPE = "DataType";
	private static final String PATH = "Path";

	@Override
	public void createObjectType() {
		var port = namespace.createObjectTypeComponent(PORT, namespace.newNodeId(nodePathType(PORT)),
				Identifiers.Integer);
		var datatype = namespace.createObjectTypeComponent(DATATYPE, namespace.newNodeId(nodePathType(DATATYPE)),
				Identifiers.String);
		var path = namespace.createObjectTypeComponent(PATH, namespace.newNodeId(nodePathType(PATH)),
				Identifiers.String);

		typeNode = namespace.createObjectTypeNode("SourceOPCUACfg",
				namespace.newNodeId(nodePathType(SOURCERSICFG_TYPE)), port, datatype, path);
	}

	@Override
	public void createInstance(Object forNode, Folders folders) {
		if (!(forNode instanceof SourceRSICfg sourceRSIcfg))
			return;

		try {
			String id = sourceRSIcfg.getId();
			var uon = namespace.createObjectNode(typeNode, id, namespace.newNodeId(nodePathInst(id)),
					folders.getConfigFolder());

			namespace.setObjectNodeComponent(uon, PORT, new Variant(sourceRSIcfg.getPort()));
			namespace.setObjectNodeComponent(uon, DATATYPE, new Variant(sourceRSIcfg.getDatatype().toString()));
			namespace.setObjectNodeComponent(uon, PATH, new Variant(sourceRSIcfg.getPath()));

			addRootNode(forNode, uon);
		} catch (UaException e) {
			log.info("Exception during creation of NodeInstance {}: {}", sourceRSIcfg.getId(), e);
		}

	}
}
