package de.dlr.bt.stc.source.rsi.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class RSISender {
	public static final String TESTMESSAGE = """
			 <Rob TYPE="KUKA">
			    <RIst X="0.0" Y="1.0" Z="2.0" A="3.0" B="4.0" C="5.5" />
			    <AIPos A1="0.0" A2="0.0" A3="0.0" A4="0.0" A5="0.0" A6="0.0" />
			    <IPOC>123645634563</IPOC>
			    <Mode>1</Mode>
			    <Offset>0</Offset>
			</Rob>""";

	public static void main(String[] args) throws IOException {
		sendMessage(TESTMESSAGE, 12345, 2);
	}

	static void sendMessage(String message, int port, int repetitions) throws IOException {
		try (DatagramChannel dc = DatagramChannel.open()) {
			SocketAddress sa = new InetSocketAddress("127.0.0.1", port);
			ByteBuffer bb = ByteBuffer.wrap(message.getBytes());

			for (int i = 0; i < repetitions; i++)
				dc.send(bb, sa);
		}
	}
}
