package de.dlr.bt.stc.task;

import org.apache.commons.configuration2.ex.ConfigurationException;

import de.dlr.bt.stc.exceptions.STCException;

public interface ITask {
	default void initializeTask() throws ConfigurationException {
	}

	void startTask() throws STCException;

	void stopTask();

	default void joinTask() throws InterruptedException {
	}
}
