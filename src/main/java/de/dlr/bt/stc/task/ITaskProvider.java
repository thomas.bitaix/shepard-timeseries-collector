package de.dlr.bt.stc.task;

import java.util.Map;

public sealed interface ITaskProvider permits ISourceProvider, IBridgeProvider, ISinkProvider {
	/**
	 * Initialize all tasks provided by the TaskProvider. Tasks are initialized
	 * independently of each other, i.e. a failing task does not prevent other tasks
	 * from being initialized
	 * 
	 * This method can be called multiple times and should try to re-initialize
	 * tasks that have failed previously or which have been cleaned up earlier
	 * 
	 * @return True, if all tasks initialized properly, false if at least one task
	 *         failed
	 */
	boolean initializeTasks();

	/**
	 * Start all Tasks provided by the TaskProvider
	 * 
	 * This method can be called multiple times and should try to start tasks that
	 * could not be started previously, or that have been stopped
	 * 
	 * @return True, if all tasks started properly, false if at least one task
	 *         failed to start
	 */
	boolean startTasks();

	/**
	 * Stop all tasks provided by this TaskProvider
	 */
	void stopTasks();

	/**
	 * Cleanup remnants of tasks
	 */
	void cleanupTasks();

	/**
	 * Map of all tasks provided by this TaskProvider
	 * 
	 * @return Map&lt;String, ITask&gt; Key is the configured name of the task (e.g.
	 *         source, sink, ...), the value is the {@link ITask} responsible for
	 *         given configuration item.
	 */
	Map<String, ITask> getTasks();
}
