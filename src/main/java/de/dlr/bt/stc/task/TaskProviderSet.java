package de.dlr.bt.stc.task;

import java.util.ArrayList;
import java.util.List;

import lombok.Value;

@Value
public class TaskProviderSet {
	private List<ISourceProvider> sources = new ArrayList<>();
	private List<IBridgeProvider> bridges = new ArrayList<>();
	private List<ISinkProvider> sinks = new ArrayList<>();

	public void addSource(ISourceProvider source) {
		sources.add(source);
	}

	public void addBridge(IBridgeProvider bridge) {
		bridges.add(bridge);
	}

	public void addSink(ISinkProvider sink) {
		sinks.add(sink);
	}

	public void addTaskProvider(ITaskProvider provider) {
		// Could be refactored to switch once pattern matching becomes generally
		// available
		if (provider instanceof ISourceProvider sp)
			addSource(sp);
		else if (provider instanceof IBridgeProvider bp)
			addBridge(bp);
		else if (provider instanceof ISinkProvider sp)
			addSink(sp);
	}
}
