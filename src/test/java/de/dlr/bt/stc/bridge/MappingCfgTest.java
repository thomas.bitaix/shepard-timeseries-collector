package de.dlr.bt.stc.bridge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

public class MappingCfgTest extends BaseTestCase {

	@Mock
	private HierarchicalConfiguration<?> config;

	@Test
	void testGetMeasurement() {
		when(config.getString("measurement")).thenReturn("meas");
		var cfg = new MappingCfg(config);
		assertEquals("meas", cfg.getMeasurement());
	}

	@Test
	void testGetLocation() {
		when(config.getString("location")).thenReturn("loc");
		var cfg = new MappingCfg(config);
		assertEquals("loc", cfg.getLocation());
	}

	@Test
	void testGetDevice() {
		when(config.getString("device")).thenReturn("dev");
		var cfg = new MappingCfg(config);
		assertEquals("dev", cfg.getDevice());
	}

	@Test
	void testGetSymbolicName() {
		when(config.getString("symbolic_name")).thenReturn("symName");
		var cfg = new MappingCfg(config);
		assertEquals("symName", cfg.getSymbolicName());
	}

	@Test
	void testGetField() {
		when(config.getString("field", "")).thenReturn("value");
		var cfg = new MappingCfg(config);
		assertEquals("value", cfg.getField());
	}

}
