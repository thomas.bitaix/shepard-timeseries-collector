package de.dlr.bt.stc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.function.Consumer;

import org.apache.commons.lang3.RandomStringUtils;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.rsi.RSIClient;
import de.dlr.bt.stc.source.rsi.RSIClientCfg;
import de.dlr.bt.stc.source.rsi.RSISource;
import de.dlr.bt.stc.source.rsi.RSITestConstants;
import de.dlr.bt.stc.source.rsi.SourceRSICfg;
import de.dlr.bt.stc.source.rsi.SourceRSICfg.SourceDataType;

class RSIConnectionTest extends BaseTestCase {

	static final String LOCALHOST = "127.0.0.1";

	static final int UDP_PORT = 12345;

	@Mock
	SourceRSICfg config;

	@Mock
	EventBus eventBus;

	RSIClient client;

	@BeforeEach
	void setup() {
		when(config.getPort()).thenReturn(UDP_PORT);
		try {
			client = new RSIClient(new RSIClientCfg(config));
		} catch (SourceConfigurationException sce) {
			fail(sce);
		}
	}

	@AfterEach
	void teardown() {
		client.cleanup();
	}

	@Test
	void testClientMessage() {
		@SuppressWarnings("unchecked")
		Consumer<String> consumer = mock(Consumer.class);
		client.registerMessageListener(consumer);
		String message = RandomStringUtils.random(30, true, true);

		try {
			sendMessage(message, UDP_PORT, 2);
			verify(consumer, timeout(1000).times(2)).accept(message);
		} catch (IOException ex) {
			fail(ex);
		}
	}

	@Test
	void testSourceConnection() throws Exception {
		when(config.getPath()).thenReturn("/Rob/IPOC");
		when(config.getDatatype()).thenReturn(SourceDataType.INTEGER);
		when(config.getPort()).thenReturn(RSIConnectionTest.UDP_PORT);
		RSISource rsiSource = new RSISource(config, eventBus);
		rsiSource.setClient(client);

		rsiSource.startTask();

		RSIConnectionTest.sendMessage(RSITestConstants.TESTMESSAGE, RSIConnectionTest.UDP_PORT, 1);

		ArgumentCaptor<DataAvailableEvent> dataCaptor = ArgumentCaptor.forClass(DataAvailableEvent.class);
		verify(eventBus, timeout(1000).times(1)).post(dataCaptor.capture());

		assertEquals(Long.valueOf(123645634563l), dataCaptor.getValue().getValue());

		rsiSource.stopTask();
	}

	static void sendMessage(String message, int port, int repetitions) throws IOException {
		try (DatagramChannel dc = DatagramChannel.open()) {
			SocketAddress sa = new InetSocketAddress(LOCALHOST, port);
			ByteBuffer bb = ByteBuffer.wrap(message.getBytes());

			for (int i = 0; i < repetitions; i++)
				dc.send(bb, sa);

		} catch (IOException ex) {
			throw ex;
		}
	}
}
