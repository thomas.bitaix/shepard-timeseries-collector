package de.dlr.bt.stc.sink;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

class ASinkCfgTest extends BaseTestCase {

	@Mock
	private HierarchicalConfiguration<?> config;

	private class TestSinkCfg extends ASinkCfg {

		protected TestSinkCfg(HierarchicalConfiguration<?> config) {
			super(config);
		}
	}

	@Test
	void testGetField() {
		when(config.getString("field", "")).thenReturn("value");
		var cfg = new TestSinkCfg(config);
		assertEquals("value", cfg.getField());
	}

}
