package de.dlr.bt.stc.sink;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.entities.DataPoint;
import de.dlr.bt.stc.entities.Mapping;
import de.dlr.bt.stc.eventbus.CacheFullEvent;

class ASinkTest extends BaseTestCase {

	private class TestSink extends ASink {

		boolean validate;

		public TestSink(String key, EventBus eventBus, String field, boolean validate) {
			super(key, eventBus, field);
			this.validate = validate;
		}

		public TestSink(String key, EventBus eventBus, String field) {
			super(key, eventBus, field);
			this.validate = true;
		}

		@Override
		protected void handleEvent(CacheFullEvent event) {
			eventBus.post(event);
		}

		@Override
		protected boolean validate() {
			return validate;
		}

	}

	@Mock
	private EventBus eventBus;

	@Test
	void initializeTaskTest_valid() throws ConfigurationException {
		var sink = new TestSink("key", eventBus, "field", true);
		assertDoesNotThrow(() -> sink.initializeTask());
	}

	@Test
	void initializeTaskTest_invalid() throws ConfigurationException {
		var sink = new TestSink("key", eventBus, "field", false);
		assertThrows(ConfigurationException.class, () -> sink.initializeTask());
	}

	@Test
	void startTaskTest() {
		var sink = new TestSink("key", eventBus, "field");
		sink.startTask();
		verify(eventBus).register(sink);
	}

	@Test
	void stopTaskTest() {
		var sink = new TestSink("key", eventBus, "field");
		sink.stopTask();
		verify(eventBus).unregister(sink);
	}

	@Test
	void eventTest() {
		var sink = new TestSink("key", eventBus, "field");
		var data = List.of(new DataPoint(1, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		sink.onCacheFullEvent(event);
		sink.handleEvents();
		verify(eventBus).post(event);
	}

	@Test
	void eventTest_noEvent() {
		var sink = new TestSink("key", eventBus, "field");
		sink.handleEvents();
		verify(eventBus, never()).post(any());
	}

	@Test
	void eventTest_wrongKey() {
		var sink = new TestSink("key", eventBus, "field");
		var data = List.of(new DataPoint(1, 2));
		var event = new CacheFullEvent("wrong", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		sink.onCacheFullEvent(event);
		sink.handleEvents();
		verify(eventBus, never()).post(any());
	}

	@ParameterizedTest
	@MethodSource
	void decideForFieldTest(String sinkField, String bridgeField, String expected) {
		var sink = new TestSink("key", eventBus, sinkField);
		var actual = sink.decideForField(bridgeField);
		assertEquals(actual, expected);
	}

	private static Stream<Arguments> decideForFieldTest() {
		//@formatter:off
        return Stream.of(
                Arguments.of("sinkField", "bridgeField", "bridgeField"),
                Arguments.of("sinkField", null, "sinkField"),
                Arguments.of("sinkField", "", "sinkField"),
                Arguments.of(null, "bridgeField", "bridgeField"),
                Arguments.of("", "bridgeField", "bridgeField"),
                Arguments.of(null, null, null),
                Arguments.of("", null, null),
                Arguments.of(null, "", null),
                Arguments.of("", "", null)
              );
		//@formatter:on
	}

	@Test
	void getQueueSizeTest() {
		var sink = new TestSink("key", eventBus, "field");
		assertEquals(0, sink.getQueueSize());

		var data = List.of(new DataPoint(1, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		sink.onCacheFullEvent(event);
		assertEquals(1, sink.getQueueSize());

		sink.handleEvents();
		assertEquals(0, sink.getQueueSize());
	}

}
