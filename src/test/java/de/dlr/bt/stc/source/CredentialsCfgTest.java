package de.dlr.bt.stc.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

class CredentialsCfgTest extends BaseTestCase {
	@Mock
	private HierarchicalConfiguration<?> config;
	@Mock
	private HierarchicalConfiguration<?> config2;

	@Test
	void testGetUsername() {
		when(config.getString("username")).thenReturn("Username");
		var cfg = new CredentialsCfg(config);
		assertEquals("Username", cfg.getUsername());
	}

	@Test
	void testGetPassword() {
		when(config.getString("password")).thenReturn("Password");
		var cfg = new CredentialsCfg(config);
		assertEquals("Password", cfg.getPassword());
	}

	@Test
	void testEqualsAndHash() {
		when(config.getString("username")).thenReturn("Username");
		when(config.getString("password")).thenReturn("Password");

		when(config2.getString("username")).thenReturn("Username");
		when(config2.getString("password")).thenReturn("Password");

		var cfg1 = new CredentialsCfg(config);
		var cfg2 = new CredentialsCfg(config2);

		assertEquals(cfg1, cfg2);
		assertEquals(cfg1.hashCode(), cfg2.hashCode());

	}

}
