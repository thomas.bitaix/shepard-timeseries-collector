package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.dlr.bt.stc.config.ACfg;
import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.eventbus.TaskModifiedEvent;
import de.dlr.bt.stc.eventbus.TaskModifiedEvent.TaskModificationType;
import de.dlr.bt.stc.source.rsi.SourceRSICfg;
import de.dlr.bt.stc.task.ITask;
import de.dlr.bt.stc.task.TaskProviderFactory;

class MQTTSourceProviderTest {

  @Test
  void initializeTasks_check_sources_created() throws MqttException {
    var sut = new MQTTSourceProvider(
        createMockedConfigManager(createConfigurations()), createMockedMqttFactory());
    
    sut.initializeTasks();
    assertEquals(4, sut.getTasks().size());
    assertNotNull(sut.getTasks().get("source-1"));
    assertNotNull(sut.getTasks().get("source-2"));
    assertNotNull(sut.getTasks().get("source-3"));
    assertNotNull(sut.getTasks().get("source-4"));
  }

  @Test
  void initializeTasks_check_sources_created_with_shared_clients() throws MqttException {
    
    var sut = new MQTTSourceProvider(createMockedConfigManager(
        createConfigsWithDifferentEndpoints()), 
        mockFactoryForSharedClients());
    
    sut.initializeTasks();
    
    assertEquals(4, sut.getTasks().size());
    
    MqttClient clientSource1 = ((MqttSource)sut.getTasks().get("source-1")).getMqttClient();
    MqttClient clientSource2 = ((MqttSource)sut.getTasks().get("source-2")).getMqttClient();
    MqttClient clientSource3 = ((MqttSource)sut.getTasks().get("source-3")).getMqttClient();
    MqttClient clientSource4 = ((MqttSource)sut.getTasks().get("source-4")).getMqttClient();
    
    //source1 and source2 share same client
    assertEquals(clientSource1, clientSource2);
    assertNotEquals(clientSource1, clientSource3);

    //source3 and source4 share same client
    assertEquals(clientSource3, clientSource4);
    assertNotEquals(clientSource2, clientSource4);
  }

  @Test
  void initializeTasks_check_initialization() throws MqttException {
    var cfg = createMockedConfigManager(createConfigurations());
    var mqttFactory = createMockedMqttFactory();
    
    var sut = new MQTTSourceProvider(cfg, mqttFactory);
    
    sut.initializeTasks();
    
    assertEquals(4, sut.getTasks().size());
    
    sut.getTasks()      
      .entrySet()
      .forEach(entry -> checkTaskWasInitialized(cfg, entry));
  }
  
  @Test
  public void register() throws MqttException {
    MQTTSourceProvider.register();
    assertNotNull(TaskProviderFactory.getInstance().getCreator(SourceMQTTCfg.class));
  }

  private void checkTaskWasInitialized(ConfigurationManager cfg, Entry<String, ITask> entry) {
    verify(cfg.getManagementEventBus()).post(
        new TaskModifiedEvent((ITask) entry.getValue(), TaskModificationType.INITIALIZE));
  }

  private MqttFactory createMockedMqttFactory() throws MqttException {
    var mqttFactory = mock(MqttFactory.class);
    var client = mock(MqttClient.class);;
    when(mqttFactory.createMqttClient(any(), any())).thenReturn(client);
    return mqttFactory;
  }

  private MqttFactory mockFactoryForSharedClients() throws MqttException {
    var mqttFactory = mock(MqttFactory.class);
    var client1 = mock(MqttClient.class);
    var client2 = mock(MqttClient.class);
    when(mqttFactory.createMqttClient(any(), eq("mqtt://ep-1"))).thenReturn(client1);
    when(mqttFactory.createMqttClient(any(), eq("mqtt://ep-2"))).thenReturn(client2);
    return mqttFactory;
  }

  private ConfigurationManager createMockedConfigManager(Map<String, ACfg> configurations) {
    var cfg = mock(ConfigurationManager.class);
    when(cfg.getConfigurations()).thenReturn(configurations);
    when(cfg.getInstanceEventBus()).thenReturn(mock(EventBus.class));
    when(cfg.getManagementEventBus()).thenReturn(mock(EventBus.class));
    return cfg;
  }
  
  private Map<String, ACfg> createConfigurations() {
    return Map.of(
        "source-1", createConfig("source-1", SourceMQTTCfg.class), 
        "source-2", createConfig("source-2", SourceMQTTCfg.class), 
        "source-3", createConfig("source-3", SourceMQTTCfg.class), 
        "source-4", createConfig("source-4", SourceMQTTCfg.class), 
        "source-5", createConfig("source-5", SourceRSICfg.class), 
        "source-6", createConfig("source-6", SourceRSICfg.class));
  }

  private Map<String, ACfg> createConfigsWithDifferentEndpoints() {
    return Map.of(
        "source-1", createConfig("source-1", SourceMQTTCfg.class, "mqtt://ep-1"), 
        "source-2", createConfig("source-2", SourceMQTTCfg.class, "mqtt://ep-1"), 
        "source-3", createConfig("source-3", SourceMQTTCfg.class, "mqtt://ep-2"), 
        "source-4", createConfig("source-4", SourceMQTTCfg.class, "mqtt://ep-2"),
        "source-5", createConfig("source-5", SourceRSICfg.class),
        "source-6", createConfig("source-5", SourceRSICfg.class));
  }

  private <T extends ACfg> ACfg createConfig(String id, Class<T> clazz) {
    return createConfig(id, clazz, "mqtt://example.com:1883");
  }
  
  private <T extends ACfg> ACfg createConfig(String id, Class<T> clazz, String endpoint) {
    var configMock = Mockito.mock(clazz);
    when(configMock.getId()).thenReturn(id);
    if(configMock instanceof SourceMQTTCfg mqttConf) {
      when(mqttConf.getEndpoint()).thenReturn(endpoint);
      when(mqttConf.getTopic()).thenReturn("topic");
      when(mqttConf.getQos()).thenReturn(0);
    }
    return configMock;
  }
}
