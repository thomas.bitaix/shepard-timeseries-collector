package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.CredentialsCfg;

class MqttConfigValidatorTest {

  private static final String ENDPOINT = "tcp://localhost:1234567";

  private static final String TOPIC = "TOPIC/1";

  private static final String SOURCE_KEY = "SOURCE_KEY_1";

  private static final String USERNAME = "user";

  private static final String PASSWORD = "password";

  private static final int QUALITY_OF_SERVICE = 0;

  @Test
  void validateSourceConfig_null() throws SourceConfigurationException {
    assertThrows(SourceConfigurationException.class,
        () -> MqttConfigValidator.validateSourceConfiguration(null));
    assertDoesNotThrow(() -> MqttConfigValidator.validateSourceConfiguration(mockConfig()));
  }

  @Test
  void validateSourceConfig_endpoint() throws SourceConfigurationException {
    var mockConfig = mockConfig();
    when(mockConfig.getEndpoint()).thenReturn(null);
    assertThrows(SourceConfigurationException.class,
        () -> MqttConfigValidator.validateSourceConfiguration(mockConfig));
  }

  @Test
  void validateSourceConfig_topic() throws SourceConfigurationException {
    var mockConfig = mockConfig();
    when(mockConfig.getTopic()).thenReturn(null);
    assertThrows(SourceConfigurationException.class,
        () -> MqttConfigValidator.validateSourceConfiguration(mockConfig));
  }

  @Test
  void validateSourceConfig_id() throws SourceConfigurationException {
    var mockConfig = mockConfig();
    when(mockConfig.getId()).thenReturn(null);
    assertThrows(SourceConfigurationException.class,
        () -> MqttConfigValidator.validateSourceConfiguration(mockConfig));

  }

  private SourceMQTTCfg mockConfig() {
    CredentialsCfg credentials = Mockito.mock(CredentialsCfg.class);
    SourceMQTTCfg config = Mockito.mock(SourceMQTTCfg.class);
    when(config.getTopic()).thenReturn(TOPIC);
    when(config.getId()).thenReturn(SOURCE_KEY);
    when(config.getQos()).thenReturn(QUALITY_OF_SERVICE);
    when(config.getEndpoint()).thenReturn(ENDPOINT);
    when(config.getCredentials()).thenReturn(credentials);
    when(credentials.getUsername()).thenReturn(USERNAME);
    when(credentials.getPassword()).thenReturn(PASSWORD);
    return config;
  }

}
