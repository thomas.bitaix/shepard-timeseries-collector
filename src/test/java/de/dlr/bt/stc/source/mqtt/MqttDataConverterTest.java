package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import de.dlr.bt.stc.source.rsi.SourceRSICfg.SourceDataType;

class MqttDataConverterTest {

  protected static final Charset STRING_ENCODING = StandardCharsets.UTF_8;
  
  @Test
  void convertInteger() throws MqttException {
    Long result = (Long)MqttDataConverter
        .convertPayload("123".getBytes(), SourceDataType.INTEGER);
    
    assertEquals(123l, result);
  }

  @Test
  void convertBoolean() throws MqttException {
    Boolean result = (Boolean)MqttDataConverter
        .convertPayload("true".getBytes(), SourceDataType.BOOL);
    assertEquals(true, result);
  }

  @Test
  void convertString() throws MqttException {
    String result = (String)MqttDataConverter
        .convertPayload(" a String ".getBytes(), SourceDataType.STRING);
    assertEquals(" a String ", result);
  }

  @Test
  void convertFloat() throws MqttException {
    Double result = (Double)MqttDataConverter
        .convertPayload("1.5".getBytes(), SourceDataType.FLOAT);
    assertEquals(1.5f, result);
  }

  @Test
  void dataType_not_set() throws MqttException {
    String result = (String) MqttDataConverter
        .convertPayload("datatype not set, is a string".getBytes(), null);
    assertEquals("datatype not set, is a string", result);
  }

  @Test
  void exception() throws MqttException {
    assertThrowsExactly(RuntimeException.class, () -> MqttDataConverter
        .convertPayload("not an integer ".getBytes(), SourceDataType.INTEGER));
  }
}
