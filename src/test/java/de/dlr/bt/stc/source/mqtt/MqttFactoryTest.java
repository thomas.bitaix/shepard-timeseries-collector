package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.dlr.bt.stc.source.CredentialsCfg;

class MqttFactoryTest {

  private static final String ENDPOINT = "tcp://localhost:1234567";

  private static final String TOPIC = "TOPIC";

  private static final String SOURCE_KEY = "SOURCE_KEY_1";

  private static final String USERNAME = "user";

  private static final String PASSWORD = "password";

  private static final int QUALITY_OF_SERVICE = 0;

  private MqttFactory sut = MqttFactory.getInstance();

  @Test
  public void createMqttSubscriber() {
    var eventBus = Mockito.mock(EventBus.class);
    var client = Mockito.mock(MqttClient.class);
    var config = mockConfig();

    var subscriber = sut.createMqttSubscriber(client, config, eventBus);

    assertEquals(TOPIC, subscriber.getTopic());
    assertEquals(QUALITY_OF_SERVICE, subscriber.getQos());
    assertEquals(SOURCE_KEY, subscriber.getSourceKey());
    assertEquals(eventBus, subscriber.getEventBus());
    assertEquals(client, subscriber.getClient());
  }

  @Test
  public void createMqttClient() throws MqttException {

    var client = sut.createMqttClient("subscriberId", ENDPOINT);

    assertNotNull(client.getClientId());
    assertEquals(ENDPOINT, client.getServerURI());
  }

  @Test
  public void createMqttClientOptions() {
    var config = mockConfig();

    var options = sut.createMqttClientOptions(config);

    assertTrue(options.isAutomaticReconnect());
    assertFalse(options.isCleanSession());
    assertEquals(30, options.getConnectionTimeout());
    assertEquals(USERNAME, options.getUserName());
    assertEquals(PASSWORD, new String(options.getPassword()));
  }

  @Test
  public void createMqttClientOptions_no_credentials() {
    var config = mockConfig();
    when(config.getCredentials()).thenReturn(null);

    var options = sut.createMqttClientOptions(config);

    assertTrue(options.isAutomaticReconnect());
    assertFalse(options.isCleanSession());
    assertEquals(30, options.getConnectionTimeout());
    assertNull(options.getUserName());
    assertNull(options.getPassword());
  }


  private SourceMQTTCfg mockConfig() {
    SourceMQTTCfg config = Mockito.mock(SourceMQTTCfg.class);
    CredentialsCfg credentials = Mockito.mock(CredentialsCfg.class);
    when(config.getTopic()).thenReturn(TOPIC);
    when(config.getId()).thenReturn(SOURCE_KEY);
    when(config.getQos()).thenReturn(QUALITY_OF_SERVICE);
    when(config.getEndpoint()).thenReturn(ENDPOINT);
    when(config.getCredentials()).thenReturn(credentials);
    when(credentials.getUsername()).thenReturn(USERNAME);
    when(credentials.getPassword()).thenReturn(PASSWORD);
    return config;
  }

}
