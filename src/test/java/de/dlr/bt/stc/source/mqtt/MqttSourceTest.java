package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;

import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;

class MqttSourceTest {

  @Test
  void initializeTask() throws Exception {
    var factory = mock(MqttFactory.class);
    var eventBus = mock(EventBus.class);
    var client = mock(MqttClient.class);
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);

    mockConfig(config);
    mockFactory(factory, eventBus, client, subscriber, config);
    when(client.isConnected()).thenReturn(false);

    var sut = MqttSource.builder()
        .instanceEventBus(eventBus)
        .mqttFactory(factory)
        .taskId("taskId")
        .sourceConfig(config)
        .mqttClient(client)
        .build();

    sut.initializeTask();

    verify(factory).createMqttClientOptions(config);
    verify(factory).createMqttSubscriber(any(), eq(config), eq(eventBus));
    verify(client).connect(any());
  }

  @Test
  void initializeTask_mqtt_exception() throws Exception {
    var factory = mock(MqttFactory.class);
    var eventBus = mock(EventBus.class);
    var client = mock(MqttClient.class);    
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);

    
    mockConfig(config);
    mockFactory(factory, eventBus, client, subscriber, config);

    var sut = MqttSource.builder()
        .instanceEventBus(eventBus)
        .mqttFactory(factory)
        .taskId("taskId")
        .sourceConfig(config)
        .mqttClient(client)
        .build();

    doThrow(MqttException.class).when(client).connect(any());

    assertThrowsExactly(SourceConfigurationException.class, () -> sut.initializeTask());

  }

  @Test
  void initializeTask_client_already_connected() throws Exception {
    var factory = mock(MqttFactory.class);
    var eventBus = mock(EventBus.class);
    var client = mock(MqttClient.class);    
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);
    
    
    mockConfig(config);
    mockFactory(factory, eventBus, client, subscriber, config);
    when(client.isConnected()).thenReturn(true);
    
    var sut = MqttSource.builder()
        .instanceEventBus(eventBus)
        .mqttFactory(factory)
        .taskId("taskId")
        .sourceConfig(config)
        .mqttClient(client)
        .build();
    
    sut.initializeTask();
    
    verify(client, never()).connect(any());
  }

  @Test
  void startTask() throws Exception {
    var client = mock(MqttClient.class);
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);

    mockConfig(config);

    var sut = MqttSource.builder()
        .taskId("taskId")
        .mqttClient(client)
        .mqttSubscriber(subscriber)
        .sourceConfig(config)
        .build();

    sut.startTask();

    verify(subscriber).start();
  }

  @Test
  void startTask_client_exception() throws Exception {
    var client = mock(MqttClient.class);
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);

    doThrow(new MqttException(null)).when(subscriber).start();

    mockConfig(config);

    var sut = MqttSource.builder()
        .taskId("taskId")
        .mqttClient(client)
        .mqttSubscriber(subscriber)
        .sourceConfig(config)
        .build();

    assertThrows(STCException.class, () -> sut.startTask());
  }

  @Test
  void stopTask() throws SourceConfigurationException, MqttException {
    var client = mock(MqttClient.class);
    var subscriber = mock(MqttSubscriber.class);

    var sut = MqttSource.builder()
        .taskId("taskId")
        .mqttClient(client)
        .mqttSubscriber(subscriber)
        .build();

    sut.stopTask();

    verify(client).disconnect();;
    verify(client).close();
  }

  @Test
  void stopTask_client_exception() throws Exception {
    var client = mock(MqttClient.class);
    var subscriber = mock(MqttSubscriber.class);
    var config = mock(SourceMQTTCfg.class);

    doThrow(new MqttException(null)).when(client).disconnect();

    mockConfig(config);

    var sut = MqttSource.builder()
        .taskId("taskId")
        .mqttClient(client)
        .mqttSubscriber(subscriber)
        .sourceConfig(config)
        .build();

    sut.stopTask();
  }

  private void mockFactory(MqttFactory factory, EventBus eventBus, MqttClient client,
      MqttSubscriber subscriber, SourceMQTTCfg config) throws MqttException {
    when(factory.createMqttClient(any(), eq("topic"))).thenReturn(client);
    when(factory.createMqttSubscriber(client, config, eventBus)).thenReturn(subscriber);
  }

  private void mockConfig(SourceMQTTCfg config) {
    when(config.getEndpoint()).thenReturn("mqtt://example.com:1883");
    when(config.getTopic()).thenReturn("topic");
    when(config.getQos()).thenReturn(0);
    when(config.getId()).thenReturn("taskId");
  }

}
