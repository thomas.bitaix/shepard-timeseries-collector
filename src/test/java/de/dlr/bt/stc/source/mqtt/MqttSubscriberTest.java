package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.nio.ByteBuffer;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import de.dlr.bt.stc.eventbus.DataAvailableEvent;

class MqttSubscriberTest {

  private static String TOPIC = "TOPIC/1";

  private static String SOURCE_KEY = "SOURCE_KEY_1";

  private static int QUALITY_OF_SERVICE = 0;

  private IMqttClient client = Mockito.mock(IMqttClient.class);

  private EventBus eventBus = Mockito.mock(EventBus.class);

  private MqttSubscriber sut;

  @BeforeEach
  public void init() {
    sut = MqttSubscriber.builder()
        .client(client)
        .eventBus(eventBus)
        .qos(QUALITY_OF_SERVICE)
        .sourceKey(SOURCE_KEY)
        .topic(TOPIC)
        .build();
  }

  @Test
  void messageArrived() throws MqttException {
    checkMessageArrived(sut, "a String".getBytes());
    checkMessageArrived(sut, "{ text: 'A JSON String'}".getBytes());

    byte[] bytesOfInteger = ByteBuffer.allocate(4).putInt(123).array();
    checkMessageArrived(sut, bytesOfInteger);
  }

  @Test
  void connectComplete_reload() throws MqttException {
    sut.connectComplete(true, SOURCE_KEY);

    verify(client).subscribe(TOPIC, QUALITY_OF_SERVICE, sut);
    verify(client).setCallback(sut);
  }

  @Test
  void connectComplete_no_reload() throws MqttException {
    sut.connectComplete(false, SOURCE_KEY);

    verify(client, never()).subscribe(TOPIC, QUALITY_OF_SERVICE, sut);
  }

  @Test
  void no_exception_on_log_methods() throws MqttException {
    sut.connectionLost(null);
    sut.deliveryComplete(null);
  }

  @Test
  void close() throws MqttException {
    sut.close();

    verify(client).disconnect();
    verify(client).close();
  }

  private void checkMessageArrived(MqttSubscriber subscriber, byte[] payload) throws MqttException {
    subscriber.messageArrived(TOPIC, createMessage(payload));

    var captor = ArgumentCaptor.forClass(DataAvailableEvent.class);

    verify(eventBus).post(captor.capture());
    reset(eventBus);

    checkEvent(payload, captor.getValue());
  }

  private void checkEvent(byte[] payload, DataAvailableEvent event) {
    assertEquals(new String(payload), event.getValue());
    assertNotNull(event.getTimestamp());
    assertNotNull(event.getVariableMap());
    assertEquals(SOURCE_KEY, event.getSourceKey());
  }

  private MqttMessage createMessage(byte[] payload) {
    var message = new MqttMessage();
    message.setId(10);
    message.setQos(QUALITY_OF_SERVICE);
    message.setPayload(payload);
    return message;
  }
}
