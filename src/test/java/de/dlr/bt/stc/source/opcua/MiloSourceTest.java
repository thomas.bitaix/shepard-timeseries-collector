package de.dlr.bt.stc.source.opcua;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.exceptions.SourceException;
import de.dlr.bt.stc.source.opcua.NodeItems.NodeItem;
import de.dlr.bt.stc.source.opcua.SourceOPCUACfg.CaptureMode;

class MiloSourceTest extends BaseTestCase {

	@Mock
	MiloClient client;

	@Mock
	EventBus eventBus;

	@Mock
	SourceOPCUACfg cfg;

	MiloSource ms;

	@BeforeEach
	void setupSource() {
		ms = new MiloSource(cfg, eventBus);
		ms.setClient(client);
	}

	@Test
	void testNoNodeConfigured() {
		assertThrows(SourceConfigurationException.class, ms::initializeTask);
	}

	@Test
	void testSingleNode() throws SourceConfigurationException, SourceException {
		NodeId id = new NodeId(0, "test");
		when(cfg.getNodeId()).thenReturn(id.toParseableString());
		when(client.readNodeValue(id)).thenReturn(new DataValue(StatusCode.GOOD));
		ms.initializeTask();
		verify(client).readNodeValue(id);

		assertEquals(1, ms.getNodes().size());
		assertTrue(ms.getNodes().keySet().contains(id));
	}

	@Test
	void testNodePath() throws SourceConfigurationException, SourceException {
		NodeId id = new NodeId(0, "test");
		NodeId id2 = new NodeId(0, "test2");
		String path = "Server/{{ .* }}";
		when(cfg.getNodePath()).thenReturn(path);

		NodeItems ni = new NodeItems();
		ni.put(id, new NodeItem());
		ni.put(id2, new NodeItem());

		when(client.resolveNodePath(path)).thenReturn(ni);
		ms.initializeTask();
		verify(client).resolveNodePath(path);

		assertEquals(ni.size(), ms.getNodes().size());
		assertTrue(ms.getNodes().keySet().contains(id));
		assertTrue(ms.getNodes().keySet().contains(id2));
	}

	@Test
	void testSubscription() throws Exception {
		NodeId id = new NodeId(0, "test");
		when(cfg.getNodeId()).thenReturn(id.toParseableString());
		when(cfg.getCaptureMode()).thenReturn(CaptureMode.SUBSCRIPTION);
		when(cfg.getPollingInterval()).thenReturn(500);
		when(client.readNodeValue(id)).thenReturn(new DataValue(StatusCode.GOOD));
		ms.initializeTask();
		ms.startTask();

		List<NodeId> ids = new ArrayList<>();
		ids.add(id);

		@SuppressWarnings("unchecked")
		ArgumentCaptor<Collection<NodeId>> nodes = ArgumentCaptor.forClass(Collection.class);

		// Nodes are registered properly
		verify(client).registerNodeSubscription(eq(ms), any(), nodes.capture());
		assertTrue(nodes.getValue().contains(id));

		// First polling is during setup, second due to outstanding subscription value
		// on mock client
		verify(client, timeout(2000).atLeast(2)).readNodeValue(id);

		// Number of readNodeValue calls minus one for setup
		int invoc = (int) Mockito.mockingDetails(client).getInvocations().stream()
				.filter(inv -> inv.getMethod().getName().equals("readNodeValue")).count() - 1;

		var mi = mock(UaMonitoredItem.class);
		var ri = mock(ReadValueId.class);
		when(mi.getReadValueId()).thenReturn(ri);
		when(ri.getNodeId()).thenReturn(id);

		ms.onSubscriptionValue(mi, new DataValue(new Variant(12345L), StatusCode.GOOD));

		ArgumentCaptor<DataAvailableEvent> daec = ArgumentCaptor.forClass(DataAvailableEvent.class);
		// One additional message should have been created for onSubscriptionValue call
		verify(eventBus, atLeast(invoc + 1)).post(daec.capture());
		assertEquals(12345L, daec.getValue().getValue());

	}

	@Test
	void testPolling() throws Exception {
		NodeId id = new NodeId(0, "test");
		when(cfg.getNodeId()).thenReturn(id.toParseableString());
		when(cfg.getCaptureMode()).thenReturn(CaptureMode.POLLING);
		when(cfg.getPollingInterval()).thenReturn(200); // Poll all 200ms
		when(client.readNodeValue(id)).thenReturn(new DataValue(new Variant(12345L), StatusCode.GOOD));

		ms.initializeTask();
		ms.startTask();
		verify(client, timeout(1500).atLeast(3)).readNodeValue(id);
		ms.stopTask();
		ms.joinTask();

		ArgumentCaptor<DataAvailableEvent> daec = ArgumentCaptor.forClass(DataAvailableEvent.class);
		verify(eventBus, atLeast(2)).post(daec.capture());
		for (var dae : daec.getAllValues())
			assertEquals(12345L, dae.getValue());
	}

	@Test
	void testUninitialized() {
		assertThrows(SourceException.class, ms::startTask);
	}

}
