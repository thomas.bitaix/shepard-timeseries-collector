package de.dlr.bt.stc.source.opcua;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.source.opcua.SourceOPCUACfg.CaptureMode;

class SourceOPCUACfgTest extends BaseTestCase {
	@Mock
	HierarchicalConfiguration<?> config;

	SourceOPCUACfg cfg;

	@BeforeEach
	void setup() {
		cfg = new SourceOPCUACfg(config);
	}

	@Test
	void getSamplingInterval() {
		when(config.getDouble(eq("sampling_interval"), any())).thenReturn(1.5);
		assertEquals(1.5, cfg.getSamplingInterval());
	}

	@Test
	void getPublishingInterval() {
		when(config.getDouble("publishing_interval", null)).thenReturn(2.5);
		assertEquals(2.5, cfg.getPublishingInterval());
	}

	@Test
	void getQueueSize() {
		when(config.getInteger("queue_size", null)).thenReturn(5);
		assertEquals(5, cfg.getQueueSize());
	}

	@Test
	void getPollingInterval() {
		when(config.getInteger("polling_interval", null)).thenReturn(6);
		assertEquals(6, cfg.getPollingInterval());
	}

	@Test
	void getSubstitutionValue() {
		when(config.getString("substitution_value", null)).thenReturn("abc");
		assertEquals("abc", cfg.getSubstitutionValue());
	}

	@Test
	void getNodeId() {
		when(config.getString("nodeid", null)).thenReturn("ns=0;i=4");
		assertEquals("ns=0;i=4", cfg.getNodeId());
	}

	@Test
	void getNodePath() {
		when(config.getString("node_path", null)).thenReturn("nodePath");
		assertEquals("nodePath", cfg.getNodePath());
	}

	@Test
	void getCaptureMode() {
		when(config.getString("capture_mode", "subscription")).thenReturn("polling");
		assertEquals(CaptureMode.POLLING, cfg.getCaptureMode());
		when(config.getString("capture_mode", "subscription")).thenReturn("subscription");
		assertEquals(CaptureMode.SUBSCRIPTION, cfg.getCaptureMode());

	}

	@Test
	void getSecurityPolicy() {
		when(config.getString(eq("security_policy"), any())).thenReturn("None");
		assertEquals(SecurityPolicy.None, cfg.getSecurityPolicy());
		when(config.getString(eq("security_policy"), any())).thenReturn("Basic256Sha256");
		assertEquals(SecurityPolicy.Basic256Sha256, cfg.getSecurityPolicy());
	}

	@Test
	void getKeystorePath() {
		when(config.getString(eq("keystore_path"), any())).thenReturn("keystore");
		assertEquals("keystore", cfg.getKeystorePath());
	}

	@Test
	void getKeystorePassword() {
		when(config.getString(eq("keystore_password"), any())).thenReturn("password");
		assertEquals("password", cfg.getKeystorePassword());
	}

	@Test
	void isCreateKeystore() {
		when(config.getBoolean("keystore_create", true)).thenReturn(true);
		assertEquals(true, cfg.isCreateKeystore());
	}

}
