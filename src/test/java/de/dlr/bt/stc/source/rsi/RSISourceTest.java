package de.dlr.bt.stc.source.rsi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.rsi.SourceRSICfg.SourceDataType;

class RSISourceTest extends BaseTestCase {

	@Mock
	EventBus eventBus;

	@Mock
	SourceRSICfg config;

	@Test
	void testParsingAttributes() throws Exception {
		when(config.getPath()).thenReturn("/Rob/RIst/@*");
		when(config.getDatatype()).thenReturn(SourceDataType.FLOAT);

		RSISource rsiSource = new RSISource(config, eventBus);

		rsiSource.receiveMessage(RSITestConstants.TESTMESSAGE);

		ArgumentCaptor<DataAvailableEvent> dataCaptor = ArgumentCaptor.forClass(DataAvailableEvent.class);
		verify(eventBus, times(6)).post(dataCaptor.capture());

		Map<String, Double> expected = new HashMap<>();
		expected.put("X", 0.0);
		expected.put("Y", 1.0);
		expected.put("Z", 2.0);
		expected.put("A", 3.0);
		expected.put("B", 4.0);
		expected.put("C", 5.5);

		for (var call : dataCaptor.getAllValues()) {
			var attrname = call.getVariableMap().get(RSISource.VARMAP_NODENAME);
			assertEquals(expected.get(attrname), call.getValue());
		}
	}

	@Test
	void testParsingElement() throws Exception {
		when(config.getPath()).thenReturn("/Rob/IPOC");
		when(config.getDatatype()).thenReturn(SourceDataType.INTEGER);
		RSISource rsiSource = new RSISource(config, eventBus);

		rsiSource.receiveMessage(RSITestConstants.TESTMESSAGE);

		ArgumentCaptor<DataAvailableEvent> dataCaptor = ArgumentCaptor.forClass(DataAvailableEvent.class);
		verify(eventBus, times(1)).post(dataCaptor.capture());

		assertEquals(Long.valueOf(123645634563l), dataCaptor.getValue().getValue());
	}

	@Test
	void testInvalidConfiguration() {
		when(config.getPath()).thenReturn("\\TEST");
		assertThrows(SourceConfigurationException.class, () -> new RSISource(config, eventBus));
	}

}
